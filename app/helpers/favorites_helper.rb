module FavoritesHelper
  def get_fav_status(message)
    unless message.favorites.empty?
      fav = message.favorites.where(user_id: current_user.id).first

      if fav.faved_at != nil
        return "fav-status-faved"
      end
    end
  
    return ""
  end
end
