module MessagesHelper
  # date & time
  def message_date(message)
    return get_date(message.created_at)
  end
  def get_date(timestamp)
    return timestamp.strftime("%d.%m.%Y")
  end
  def message_datetime(message)
    return message.created_at.strftime("%d.%m.%Y - %H:%M")
  end
  def message_time(message)
    return message.created_at.strftime("%H:%M")
  end


  # orgin
  def fix_origin(message)
    if message.origin == "messages"
      return "comments"
    end

    return message.origin
  end


  # align
  def received_or_sent(user, message)
    id_andre = ["100001426361532", "Me"]
    id_linda = ["613348845421106", "Linda Voigt", "Babe"]


    if current_user.username == "andre"
      user_array = id_andre
    else
      user_array = id_linda
    end

    if user_array.include?(message.sender_contact)
      return "message-sent"
    else
      return "message-received"
    end
  end

  def message_back_path(message)

    if request.referer.split("/").last == "favorites"
      return favorites_path
    end

    if request.referer.split("/").last == "messages"
      return messages_path
    end

    if @message.page_url.present? && @message.page_url != nil
      return messages_path(page: message.page_url.gsub('page=', ''))
    end
  end
end
