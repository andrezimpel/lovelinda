class Message < ActiveRecord::Base

  has_many :favorites

  require "open-uri"
  require "csv"

  # defualt sort scope
  default_scope { order('created_at ASC') }

  ##############################
  #
  #          SMS & iMessage
  #
  ##############################

  # import from iPhone backup
  def self.import_from_backup(path)
    CSV.foreach(Rails.root + path, headers: true) do |row|
      r = row.to_hash
      r = r.merge({"origin" => "messages"})

      save_phone_message(r)
    end
  end

  def self.save_phone_message(message)
    # data
    data = {}

    data[:foreign_id] = "1"

    data[:sender_contact] = message["Name"].strip
    data[:sender_name] = get_sender(message["Name"].strip)[1]

    data[:receiver_contact] = message["Phone Number"]
    data[:receiver_name] = get_receiver(message["Name"].strip, "messages")[1]

    data[:body] = message["Message"].strip
    data[:origin] = message["origin"]
    data[:message_type] = message["origin"]
    data[:created_at] = DateTime.parse "#{message['Date']} #{Time.zone}"

    # Message.where(body: data[:body], sender_contact: data[:sender_contact], created_at: data[:created_at])

    new_message = Message.create_with(data).find_or_create_by!(body: data[:body], sender_contact: data[:sender_contact], created_at: data[:created_at])

    return new_message
  end

  ##############################
  #
  #          FACEBOOK
  #
  ##############################

  # desc: import facebook messages
  def self.import_from_facebook
    token = "CAACEdEose0cBAPZBJgOOtFAKfXyQvIFz8zTJXK1lE2VomIPWaAqLp0GVY7HCENI9FnZB8oHGFmfLrmYMne93SB3ktjb8fdYpPGzq5CHTrPAWp37JOu2KBJ0Hi2r3seujS1ZAtSpjCUdbSbw1Rv4ZAsxZBI5G126jweux2tsLPi3IUECnvS5ao831MOZAHW6w055Le3SXrmBzgZCy1sYNN86jAl2vIKj9FwZD";
    url = "https://graph.facebook.com/v2.2/555657294533057?access_token=#{token}"

    # parse_facebook_data(url)

    # parse data
    data = URI.parse(url).read
    data = JSON.parse data

    save_messages(data["comments"]["data"])
    start_paging(data["comments"]["paging"]["next"])

  end

  # parse data
  def self.parse_facebook_data(url)
    # parse data
    data = URI.parse(url).read
    data = JSON.parse data

    if data.length > 1
      save_messages(data["data"])
      start_paging(data["paging"]["next"])
    else
      return
    end
  end

  # paging
  def self.start_paging(url)
    parse_facebook_data(url)
  end

  # save message
  def self.save_messages(messages)
    for message in messages do
      self.save_message(message)
    end
  end

  def self.save_message(message)
    # data
    data = {}

    data[:foreign_id] = message["id"]
    data[:sender_contact] = message["from"]["id"].to_s
    data[:sender_name] = message["from"]["name"].to_s
    data[:receiver_contact] = get_receiver(message["from"]["id"].to_i, "facebook")[0]
    data[:receiver_name] = get_receiver(message["from"]["id"].to_i, "facebook")[1]
    data[:body] = message["message"]
    data[:origin] = "facebook"
    data[:message_type] = "facebook"
    data[:created_at] = message["created_time"]

    new_message = Message.create_with(data).find_or_create_by!(foreign_id: message["id"], created_at: message["created_time"])

    return new_message
  end

  # get receiver
  def self.get_receiver(sender, origin)
    id_andre = [100001426361532, "Me"]
    id_linda = [613348845421106, "Linda Voigt", "Babe"]

    name_andre = "Andre Zimpel"
    if origin == "facebook"
      name_linda = "Fräulein Linda"
    else
      name_linda = "Linda Voigt"
    end

    if origin == "facebook"
      if id_andre.include?(sender)
        return [id_linda[0], name_linda]
      elsif id_linda.include?(sender)
        return [id_andre[0], name_andre]
      end
    else
      if id_andre.include?(sender)
        return [id_linda[1], name_linda]
      elsif id_linda.include?(sender)
        return [id_andre[1], name_andre]
      end
    end
  end

  # get sender
  def self.get_sender(sender)
    id_andre = [100001426361532, "Me"]
    id_linda = [613348845421106, "Linda Voigt", "Babe"]

    name_andre = "Andre Zimpel"
    name_linda = "Linda Voigt"

    if id_andre.include?(sender)
      return [id_andre[1], name_andre]
    elsif id_linda.include?(sender)
      return [id_linda[1], name_linda]
    end
  end
end
