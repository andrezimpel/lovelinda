class Favorite < ActiveRecord::Base

  belongs_to :message
  belongs_to :user

  # defualt sort scope
  default_scope { order('created_at DESC') }

  scope :faved, lambda { where(['faved_at IS NOT NULL']) }

  before_save :trigger_fav

  def trigger_fav
    if self.faved_at == nil && self.unfaved_at == nil
      self.faved_at = DateTime.now
    elsif self.faved_at != nil && self.unfaved_at == nil
      self.unfaved_at = DateTime.now
      self.faved_at = nil
    elsif self.faved_at == nil && self.unfaved_at != nil
      self.unfaved_at = nil
      self.faved_at = DateTime.now
    end
  end
end
