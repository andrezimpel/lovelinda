var refresh = function() {

  if ($.cookie(decodeURIComponent(window.location.search))) {
    $(".content").scrollTop($.cookie(decodeURIComponent(window.location.search)));
  }

  $(".messages-listing-message").click(function(){
    save_position();
  });
}

function save_position() {
  url = window.location.pathname + window.location.search;
  params = window.location.search;
  $.cookie(params, $(".content").scrollTop(), {path: window.location.pathname});
}


$(document).on('page:load ready', refresh);
