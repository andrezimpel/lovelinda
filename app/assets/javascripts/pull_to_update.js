// var pull_to_update = function() {
//
// }

function connect_pull_to_refresh() {
  $('#content').xpull({
    'callback':function(){
      Turbolinks.visit(location.toString());
      connect_pull_to_refresh();
    }
  });
}


$(document).on('page:load page:change page:receive popstate push ready', connect_pull_to_refresh);
