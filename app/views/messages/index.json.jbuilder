json.array!(@messages) do |message|
  json.extract! message, :id, :sender_contact, :sender_name, :receiver_contact, :receiver_name, :body, :origin, :message_type
  json.url message_url(message, format: :json)
end
