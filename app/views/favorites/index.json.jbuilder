json.array!(@favorites) do |favorite|
  json.extract! favorite, :id, :message_id, :user_id, :faved_at, :unfaved_at
  json.url favorite_url(favorite, format: :json)
end
