class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :edit, :update, :destroy]

  # GET /messages
  # GET /messages.json
  def index
    @page_count = params[:page]
    @messages = Message.all.page(@page_count).per(50)
    @message_months = @messages.group_by { |t| t.created_at.beginning_of_day }

    # @messages.group_by_day(:created_at).count
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    @favorite = @message.favorites.where(user_id: current_user.id).first

    unless @favorite
      @favorite = Favorite.new
    end

    # save pagination url
    page = request.referer.split("?")[1]

    if page != nil
      @message.update(page_url: page)
    end
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message, notice: 'Message was successfully created.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  # custom
  def import_from_backup
  end
  def import_from_backup_post
    path = params[:file].path
    Message.delay.import_from_backup(path)
    redirect_to root_path, notice: "Importing Messages."
  end

  def import_from_facebook
    Message.delay.import_from_facebook
    redirect_to root_path, notice: "Importing Messages."
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:sender_contact, :sender_name, :receiver_contact, :receiver_name, :body, :origin, :message_type, :foreign_id, :page_url)
    end
end
