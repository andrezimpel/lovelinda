set :application, 'love'
set :repo_url, 'git@bitbucket.org:andrezimpel/lovelinda.git'


# set :deploy_to, '/var/www/my_app'
set :deploy_to, '/var/www/love.andrezimpel.com' # production
# set :scm, :git

# user
set :use_sudo, true

set :rvm_ruby_version, '2.1.3p242'
set :rvm_type, :user

set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_dirs, [
  "log",
  "restricted",
  "public/system",
  "tmp/cache",
  "tmp/pids",
  "tmp/sockets",
  "vendor/bundle",
]

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
    end
  end

  after :restart, :restart_passenger do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      within release_path do
        execute :touch, 'tmp/restart.txt'
      end
    end
  end


  after :finishing, 'deploy:restart_passenger'
  after :finishing, 'deploy:cleanup'
  # after :finishing, 'deploy:write_crontab'
end
