class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.integer :message_id
      t.integer :user_id
      t.datetime :faved_at
      t.datetime :unfaved_at

      t.timestamps null: false
    end
  end
end
