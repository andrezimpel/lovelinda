class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :sender_contact
      t.string :sender_name
      t.string :receiver_contact
      t.string :receiver_name
      t.text :body
      t.string :origin
      t.string :message_type

      t.timestamps null: false
    end
  end
end
