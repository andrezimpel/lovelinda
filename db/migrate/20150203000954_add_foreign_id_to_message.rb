class AddForeignIdToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :foreign_id, :string
  end
end
